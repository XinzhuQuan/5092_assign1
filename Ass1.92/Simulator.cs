﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4
{
    public class Simulator
    {
        public double S;
        public double K;
        public double R;
        public double Sigma;
        public double T;
        public double trail;
        public static double[] Mon()
        {
            //double[,] arr = new double[CommonValue.trail, Convert.ToInt32(CommonValue.Time) * 252];
            double sum = 0;
            double sum_delta = 0;
            double sum_deltain = 0;
            double sum_deltade = 0;
            double sum_gamma = 0;
            double sum_vega = 0;
            double sum_rho = 0; 
            double sum_theta = 0;
            double sum_ave = 0;
            double sum_a = 0;





            for (int i = 0; i < CommonValue.trail; i++)
            {

                European Eu = new European(i);//sum every trails' price
                Eu.GetPrice();
                sum = sum + Eu.price;// use eu.price
                sum_deltain = sum_deltain + Eu.price_in;//same
                sum_deltade = sum_deltade + Eu.price_de;
                sum_theta =sum_theta + Eu.price_theta;
                sum_vega = sum_vega + Eu.price_vega;
                sum_rho = sum_rho + Eu.price_rho;
                


            }

            sum_ave = sum / CommonValue.trail;//calculate the averate
            sum = sum * Math.Exp(-CommonValue.Time * CommonValue.Rate) / CommonValue.trail;//discount
            for (int i = 0; i < CommonValue.trail; i++)
            {

                European Eu = new European(i);
                Eu.GetPrice();
                sum_a = sum_a+(Eu.price - sum_ave)* (Eu.price - sum_ave);// in order to calculate the sdv 

        }
            sum_a=Math.Sqrt(sum_a / (CommonValue.trail * (CommonValue.trail - 1))) * Math.Exp(-CommonValue.Time * CommonValue.Rate);// calculate the sdv
            sum_deltain = sum_deltain* Math.Exp(-CommonValue.Time * CommonValue.Rate) / CommonValue.trail;//discount the increase price
            sum_deltade = sum_deltade * Math.Exp(-CommonValue.Time * CommonValue.Rate) / CommonValue.trail;//discount the decrease price
            sum_delta = (sum_deltain - sum_deltade) / (2 * CommonValue.increS);//calclulate the delta
            sum_gamma = (sum_deltain + sum_deltade - 2 * sum) / (CommonValue.increS * CommonValue.increS);//calculate the gamma
            sum_vega=sum_vega* Math.Exp(-CommonValue.Time * CommonValue.Rate) / CommonValue.trail;//discount
            sum_vega = (sum_vega - sum) / CommonValue.increS;//calculate
            sum_rho=sum_rho*Math.Exp(-CommonValue.Time * (CommonValue.increS + CommonValue.Rate))/ CommonValue.trail;//discount
            sum_rho = (sum_rho - sum) / CommonValue.increS;//calculate
            sum_theta = sum_theta * Math.Exp(-(CommonValue.Time+CommonValue.increR*CommonValue.Time)* CommonValue.Rate) / CommonValue.trail;//discount
            sum_theta = -(sum_theta - sum) / (CommonValue.increR);//calculate


          
            double[] result=new double[7] { sum,sum_delta,sum_gamma,sum_theta,sum_vega,sum_rho,sum_a };//output 
            return result;
        }

    }
}

