﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4;
namespace WindowsFormsApp4
{
    //create a class to put some universal variables
    static class CommonValue
    { //take the number from the users
        static public double Spotprice;
        static public double Strike;
        static public int Time;
        static public double Sigma;
        static public double Rate;
        static public int trail;
        static public int steps;
   
        static public double[,] rand;
        //random matrix
        static public double[,] St;
        //stock matrix
        static public double[,] Vst;

        static public double[,] Rst;
        static public double[,] Tst;
        static public double increS = 0.1;
        static public double increR = 0.0001;
       


        static public double type;
        static public void gene_path()//create value in the matrix
        {
            rand = new Fancyrandom().randlist;
            St = new double[CommonValue.trail, CommonValue.steps + 1];
            Vst = new double[CommonValue.trail, CommonValue.steps+1];
            Rst = new double[CommonValue.trail, CommonValue.steps+1];
            Tst = new double[CommonValue.trail, CommonValue.steps + 1];

        }
    }
}
