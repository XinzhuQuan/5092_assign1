﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4
{
    class European : Option
    {// create the option class
        public European(int a)
        {
            this.trails = a;//constrator trails are the rows
        }
        public double price = 0;
        public double price_in = 0;
        public double price_de = 0;
        public double price_theta = 0;
        public double price_delta = 0;
        public double price_vega = 0;
        public double price_rho = 0;

        public int trails;
        public double dt = Convert.ToDouble(CommonValue.Time) / Convert.ToDouble (CommonValue.steps);//calculte t
        public double dtt = Convert.ToDouble(CommonValue.Time+CommonValue.increR* CommonValue.Time) / Convert.ToDouble(CommonValue.steps);//calculte the t+0.0001t
        public void GetPrice()
        {

   
            for (int idx = 1; idx <= CommonValue.steps; idx++)
            {   //use monto carlo calculate the stock price
                CommonValue.St[trails, 0] = CommonValue.Spotprice;
                CommonValue.St[trails, idx] = CommonValue.St[trails, idx - 1] * Math.Exp(((CommonValue.Rate - 0.5 * CommonValue.Sigma * CommonValue.Sigma) * dt) + (CommonValue.Sigma * Math.Sqrt(dt) * CommonValue.rand[trails, idx-1]));
                //calculate the stock price matraix with the change of sigma
                CommonValue.Vst[trails, 0] = CommonValue.Spotprice;
                CommonValue.Vst[trails, idx] = CommonValue.Vst[trails, idx - 1] * Math.Exp(((CommonValue.Rate - 0.5 * (CommonValue.Sigma + CommonValue.increS) * (CommonValue.Sigma + CommonValue.increS))* dt) + ((CommonValue.Sigma + CommonValue.increS) * Math.Sqrt(dt) * CommonValue.rand[trails, idx - 1]));
                //calculate the stock price matrix with the change of rate
                CommonValue.Rst[trails, 0] = CommonValue.Spotprice;
                CommonValue.Rst[trails, idx] = CommonValue.Rst[trails, idx - 1] * Math.Exp((((CommonValue.Rate+CommonValue.increS) - 0.5 * CommonValue.Sigma * CommonValue.Sigma) * dt) + (CommonValue.Sigma * Math.Sqrt(dt) * CommonValue.rand[trails, idx - 1]));
                //calculate the stock price matrix with the change of t
                CommonValue.Tst[trails, 0] = CommonValue.Spotprice;
                CommonValue.Tst[trails, idx] = CommonValue.Tst[trails, idx - 1] * Math.Exp(((CommonValue.Rate - 0.5 * CommonValue.Sigma * CommonValue.Sigma) * dtt) + (CommonValue.Sigma * Math.Sqrt(dtt) * CommonValue.rand[trails, idx - 1]));
              

            }

            
            price = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps-1] - CommonValue.Strike)); //use the last column price minus strike price
            price_in = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps - 1] / CommonValue.Spotprice * (CommonValue.Spotprice + CommonValue.increS) - CommonValue.Strike));//use the last column's stock price  in order to calculate the delta
            price_de = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps - 1] / CommonValue.Spotprice * (CommonValue.Spotprice - CommonValue.increS) - CommonValue.Strike));//use the last column's stock  price in order to calculate the delta
            price_theta = Math.Max(0, CommonValue.type * (CommonValue.Tst[trails, CommonValue.steps-1] - CommonValue.Strike)); // //use the last column change of time's price minus strike price
            price_vega = Math.Max(0, CommonValue.type * (CommonValue.Vst[trails, CommonValue.steps - 1] - CommonValue.Strike));//use the last column change of volatility's price minus strike price
            price_rho = Math.Max(0, CommonValue.type * (CommonValue.Rst[trails, CommonValue.steps - 1] - CommonValue.Strike));//use the last column change of rate's price minus strike price



        }

    }
}